all: TemplateMain

TemplateMain: TemplateMain.cpp TemplateDoublyLinkedList.h
	g++-4.7 -std=c++11 TemplateMain.cpp -g -o TemplateMain

clean:
	rm TemplateMain
	
run:
	./TemplateMain
