/*
	TemplateDoublyLinkedList.h
	Matt Gaikema
*/

#include <cstdlib>
#include <iostream>
#include <stdexcept>

using namespace std;

template <typename L> class DoublyLinkedList; // class declaration

// list node
template <typename L> class DListNode {
private: L obj;
	DListNode *prev, *next;
	friend class DoublyLinkedList<L>;
public:
	DListNode(L e=L(), DListNode *p = NULL, DListNode *n = NULL)
		: obj(e), prev(p), next(n) {}
	L getElem() const { return obj; }
	DListNode* getNext() const { return next; }
	DListNode* getPrev() const { return prev; }
};

// doubly linked list
template <typename L> class DoublyLinkedList {
protected: DListNode<L> header, trailer;
public:
	DoublyLinkedList() : header(), trailer() // constructor
	{ header.next = &trailer; trailer.prev = &header; }
  
	DoublyLinkedList(const DoublyLinkedList<L>& dll); // copy constructor
  
	~DoublyLinkedList(); // destructor
  
	DoublyLinkedList& operator=(const DoublyLinkedList<L>& dll); // assignment operator
  
	// return the pointer to the first node
	DListNode<L> *getFirst() const { return header.next; } 
  
	// return the pointer to the trailer
	const DListNode<L> *getAfterLast() const { return &trailer; }
  
	// return if the list is empty
	bool isEmpty() const { return header.next == &trailer; }
  
	L first() const; // return the first object
  
	L last() const; // return the last object
  
	void insertFirst(L newobj); // insert to the first of the list
  
	L removeFirst(); // remove the first node
  
	void insertLast(L newobj); // insert to the last of the list
  
	L removeLast(); // remove the last node
  
	DListNode<L>* insertOrderly(const L& obj);
  
	bool contains(const L& obj, bool ean=false);
};

// output operator
template <typename L>
ostream& operator<<(ostream& out, const DoublyLinkedList<L>& dll);

/************************************************************
// DEFINITIONS
************************************************************/

// extend range_error from <stdexcept>
struct EmptyDLinkedListException : std::range_error {
	explicit EmptyDLinkedListException(char const* msg=NULL): range_error(msg) {}
};

// copy constructor
template <typename L>
DoublyLinkedList<L>::DoublyLinkedList(const DoublyLinkedList<L>& dll)
{
	// Initialize the list
	header.next = &trailer; 
	trailer.prev = &header;
  
	//trailer = *dll.getAfterLast();
	DListNode<L>* js = dll.getFirst();
	// Copy from dll
	while(js != dll.getAfterLast())
	{
		//DListNode* temp = new DListNode(js->getElem(), js->getPrev(), js->getNext());
		//cout << js->getElem() << endl;
		insertLast(js->getElem());
		js = js->next;
	}
}

// assignment operator
template <typename L>
DoublyLinkedList<L>& DoublyLinkedList<L>::operator=(const DoublyLinkedList<L>& dll)
{
	// Delete the whole list
	DListNode<L> *prev_node, *node = header.next;
	while(node != &trailer)
	{
		prev_node = node;
		node = node->next;
		delete prev_node;
	}
  
	// Copy from dll
	//trailer = *dll.getAfterLast();
	DListNode<L>* js = dll.getFirst();
	// Copy from dll
	while(js->next != dll.getAfterLast())
	{
		//DListNode* temp = new DListNode(js->getElem(), js->getPrev(), js->getNext());
		//cout << js->getElem() << endl;
		insertLast(js->getElem());
		js = js->next;
	}
}

// insert the object to the first of the linked list
template <typename L>
void DoublyLinkedList<L>::insertFirst(L newobj)
{ 
	DListNode<L> *newNode = new DListNode<L>(newobj, &header, header.next);
	header.next->prev = newNode;
	header.next = newNode;
}

// insert the object to the last of the linked list
template <typename L>
void DoublyLinkedList<L>::insertLast(L newobj)
{
  DListNode<L> *newNode = new DListNode<L>(newobj, trailer.prev,&trailer);
  trailer.prev->next = newNode;
  trailer.prev = newNode;
}

// remove the first object of the list
template <typename L>
L DoublyLinkedList<L>::removeFirst()
{ 
  if (isEmpty())
    throw EmptyDLinkedListException("Empty Doubly Linked List");
  DListNode<L> *node = header.next;
  node->next->prev = &header;
  header.next = node->next;
  L obj = node->obj;
  delete node;
  return obj;
}

// remove the last object of the list
template <typename L>
L DoublyLinkedList<L>::removeLast()
{
  if (isEmpty())
    throw EmptyDLinkedListException("Empty Doubly Linked List");
  DListNode<L> *node = trailer.prev;
  node->prev->next = &trailer;
  trailer.prev = node->prev;
  L obj = node->obj;
  delete node;
  return obj;
}

// destructor
template <typename L>
DoublyLinkedList<L>::~DoublyLinkedList<L>()
{
  DListNode<L> *prev_node, *node = header.next;
  while (node != &trailer) {
    prev_node = node;
    node = node->next;
    delete prev_node;
  }
  header.next = &trailer;
  trailer.prev = &header;
}

// return the first object
template <typename L>
L DoublyLinkedList<L>::first() const
{ 
  if (isEmpty())
    throw EmptyDLinkedListException("Empty Doubly Linked List");
  return header.next->obj;
}

// return the last object
template <typename L>
L DoublyLinkedList<L>::last() const
{
  if (isEmpty())
    throw EmptyDLinkedListException("Empty Doubly Linked List");
  return trailer.prev->obj;
}

// Insert elements in order
template <typename L>
DListNode<L>* DoublyLinkedList<L>::insertOrderly(const L& obj)
{
	DListNode<L>* js = new DListNode<L>(obj);

	if (isEmpty()) //|| !(header.next->obj < obj))
	{
		header.next = js;
		trailer.prev = js;
		js->prev = &header;
		js->next = &trailer;
		return header.next; 
	}
	
	// Bad hack to fix a bug where inserting an element larger
	// than anything else would not work
	if (trailer.prev->obj < js->obj)
	{
		insertLast(obj);
		delete js;
		return trailer.prev;
	}
	
	DListNode<L>* temp = header.next;
	while (temp != &trailer)
	{
		if (js->obj<temp->obj)
		{
			js->next = temp;
			js->prev = temp->prev;
			temp->prev->next = js;
			temp->prev = js;
			return js;
		}
		//js->next = temp;
		temp = temp->next;
	}
}

/**
* Return true if the list contains the element.
* @param obj: the element to search, 
* ean: whether or not the element should be displayed
*/
template <typename L>
bool DoublyLinkedList<L>::contains(const L& obj, bool ean)
{
	if(isEmpty() || header.next == NULL)
		return false;
		
	bool flag = false;
	//DListNode<L>* js = new DListNode<L>(obj);
	DListNode<L>* temp = header.next;
	while (temp!=&trailer && !flag)
	{
		if (temp->obj == obj)
		{
			flag = true;
			if (ean)
				cout << temp->obj << endl;
		}
		temp = temp->next;
		//temp->obj = temp->next->obj;	
	}
	
	return flag;
}

// return the list length
template <typename L>
int DoublyLinkedListLength(DoublyLinkedList<L>& dll) 
{
  DListNode<L> *current = dll.getFirst();
  int count = 0;
  while(current != dll.getAfterLast()) 
	{
    count++;
    current = current->getNext(); //iterate
  }
  return count;
}

// output operator
template <typename L>
ostream& operator<<(ostream& out, const DoublyLinkedList<L>& dll) 
{
  string delim = "----------------";//"<->";
	
  DListNode<L>* js = dll.getFirst();
	while (js != dll.getAfterLast())
	{
		out << js->getElem() << '\n' << delim << '\n';
		js = js->getNext();
  }
  return out;
}
